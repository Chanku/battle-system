package com.github.battlesystem;
/*
 * This is the main class, just makes sure it actually continues to run. 
 */
public class BattleCore {
	static boolean inGame = true;
	static boolean atMainMenu = true; 
	static boolean inBattle = false;
	static boolean atEnemySelect = false;
	static boolean atInventoryMenu = false;
	public static void main(String[] args){
		while(inGame == true){
			while(atMainMenu == true){
				MenuSystem.displayMainMenu();
			}
			while(atInventoryMenu == true){
				MenuSystem.displayEquipmentMenu();
			}
			while(atEnemySelect == true){
				MenuSystem.displayEnemySelect();
			}
		}
	}
}
