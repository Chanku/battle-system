package com.github.battlesystem;

import java.util.ArrayList;
/*
 * This, hopefully, will contain most of the Inventory System itself. 
 */
public class InventorySystem {
	Weapon Weapon = new Weapon();
	Excalibur Excalibur = new Excalibur();
	static ArrayList<String> inventory = new ArrayList<String>();
	public ArrayList<String> getInventory(){
		//inventory.add(Excalibur.getExcaliburName());
		 return inventory; 
	}
	public  void addToInventory(String item){
		inventory.add(item);
	}
	public void removeFromInventory(int position){
		inventory.remove(position);
	}
	public void addExcalibur(){
		inventory.add(Excalibur.getExcaliburName());
	}

}
