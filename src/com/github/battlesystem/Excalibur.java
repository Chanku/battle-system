package com.github.battlesystem;
/*
 * A class for the Excalibur Item/Weapon
 */
public class Excalibur extends Weapon{
	int[] stats = {0,2,1,1}; //hp, attack, defense, equipability
	public void addStats(){
		Player.stats[1] = Player.stats[1] + stats[1];
		Player.stats[2] = Player.stats[2] + stats[2];
	}
	public String getExcaliburName(){
		return WeaponName = "Excalibur";
	}
}
