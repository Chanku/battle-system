package com.github.battlesystem;

import java.util.Scanner;
/*
 * Much like the MenuSystem class this is I can easily change this for game integration. 
 */
public class InputSystem {
	public void mainMenuCommands(){
		Scanner scan = new Scanner(System.in);
		int playerInput= scan.nextInt();
		switch(playerInput){
		case 1:
			MenuSystem.displayText("Loading Menu");
			MenuSystem.displayText("Menu Loaded!");
			BattleCore.atEnemySelect = true;
			BattleCore.atMainMenu = false; 
			break;
		case 2:
			MenuSystem.displayText(" ");
			BattleCore.atInventoryMenu = true; 
			BattleCore.atMainMenu = false;
			break;
		default:
			MenuSystem.displayText("Unknown Input!");
		}
	}
	public void enemySelectCommands(){
		Scanner scan = new Scanner(System.in);
		int playerInput = scan.nextInt();
		switch(playerInput){
		case 1:
			MenuSystem.displayText("Rat Selected!");
			BattleCore.inGame = false;
			BattleCore.atEnemySelect = false; 
			break;
		case 2: 
			MenuSystem.displayText("Reaper Selected");
			BattleCore.inGame = false;
			BattleCore.atEnemySelect = false; 
			break;
		default:
			MenuSystem.displayText("Unknown Input!");
		}
	}
	public void InventoryEquipCommands(){
		Scanner scan = new Scanner(System.in);
		int playerInput = scan.nextInt();
		EquipSystem EquipSystem = new EquipSystem();
		switch(playerInput){
		case 1: 
			EquipSystem.addToEquip();
			BattleCore.atInventoryMenu = false;
			BattleCore.atMainMenu = true; 
			break;
		default:
			System.out.println("Unknown Input");
		}
	}
}
