package com.github.battlesystem;
/*
 * This contains the Menu System, this is so I can easily change the menus when I decide to integrate the game itself into 
 * the battle system. 
 */
public class MenuSystem {
	public static void displayText(String text){
		System.out.println(text);
	}
	static InputSystem inputSystem = new InputSystem();
	public static void displayMainMenu(){
		displayText("Welcome to Battle System! A Battle System Setup to create a battle system");
		displayText("Please select an Option below, by the number");
		displayText("1. Enemy Select");
		displayText("2. Equipment Select");
		inputSystem.mainMenuCommands();
	}
	public static void displayEnemySelect(){
		displayText("Select Your Enemy!");
		displayText("1. Rat");
		displayText("2. Reaper");
		//displayText("3. ");
		inputSystem.enemySelectCommands();
		System.out.println(Player.stats[1]);
	}
	public static void displayEquipmentMenu(){
		InventorySystem InvSys = new InventorySystem();
		InvSys.addExcalibur();
		displayText("Welcome to the Inventory Menu!");
		displayText("You may equip one item!");
		System.out.println(InvSys.getInventory());
		inputSystem.InventoryEquipCommands();
	}
	
}
